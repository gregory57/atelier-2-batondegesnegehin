<?php
session_start();
require 'vendor/autoload.php';
\Model\ConnexionDB::createDB(parse_ini_file('config.ini'));
$app = new \Slim\Slim();

$app->get(
    '/',
    function () {
        $home = new \Controller\GameController();
        $home->index();
    });

$app->get(
    '/scores',
    function () {
        $score = new \Controller\GameController();
        $score->scores();
    });

$app->get(
    '/admin',
    function () {
        $admin = new \Controller\AdminController();
        if (isset($_SESSION['admin'])) {
            $admin->dashboard();
        } else {
            $admin->login();
        }
    });
$app->post(
    '/admin',
    function () {
        $admin = new \Controller\AdminController();
        if (isset($_SESSION['admin'])) {
            $admin->actionDashboard();
        } else {
            $admin->verifLogin();
        }
    });
$app->get(
    '/admin/register',
    function () {
        $admin = new \Controller\AdminController();
        $admin->register();
    });
$app->post(
    '/admin/register',
    function () {
        $admin = new \Controller\AdminController();
        $admin->verifRegister();
    });
$app->get(
    '/admin/logout',
    function () {
        $admin = new \Controller\AdminController();
        $admin->logout();
    });

$app->group(
    '/play/games',
    function () use ($app) {
        $c = new Controller\GameController();

        $app->post("/", function() use ($app)
        {
             $app->response->headers->set('Content-Type', 'application/json');
             $partie = new \Controller\GameController();
             $partie->creerPartie(); 
        })->name("creation");

        $app->get("/:id", function($id) use($c, $app)
        {
            $app->response->headers->set('Content-Type', 'application/json');
            $c->description($id);
        })->name("description");


        $app->get(
            '/:id/photos',
            function($id) use($app){
                $photo = new \Controller\PhotoController();
                $app->response->headers->set('Content-Type', 'application/json');
                $photo->apiListPhoto($id);
            })->name('photos');
        
        $app->put("/:id", function($id) use($c, $app)
        {
            $app->response->headers->set('Content-Type', 'application/json');
            $c->update_partie($id);
        })->name("update_partie");

        $app->get(
            '/score/best',
            function () use ($app, $c) {
                $app->response->headers->set('Content-Type', 'application/json'); 
                $c->bestScores();
            })->name('best_scores');
    });




$app->group(
    '/api',
    function () use ($app) {
        $adminapi = new \Controller\ApiAdminController();
        $app->post(
            '/admin/ville_exist',
            function () use ($app, $adminapi) {
                $app->response->headers->set('Content-Type', 'application/json');
                $adminapi->villeExist();
            });

		$app->post(
            '/admin/ajout_ville',
            function () use ($app, $adminapi) {
                $app->response->headers->set('Content-Type', 'application/json');
                $adminapi->ajoutVille();
            });

        $app->post(
            '/admin/distance_difficulte',
            function () use ($app, $adminapi) {
                $app->response->headers->set('Content-Type', 'application/json');
                $adminapi->difficulteDistance();
            });
        $app->post(
            '/admin/set_distance_difficulte',
            function () use ($app, $adminapi) {
                $app->response->headers->set('Content-Type', 'application/json');
                $adminapi->setDifficulteDistance();
            });

        $app->post(
            '/admin/nbphotos_difficulte',
            function () use ($app, $adminapi) {
                $app->response->headers->set('Content-Type', 'application/json');
                $adminapi->difficulteNbPhotos();
            });
        $app->post(
            '/admin/set_nbphotos_difficulte',
            function () use ($app, $adminapi) {
                $app->response->headers->set('Content-Type', 'application/json');
                $adminapi->setDifficulteNbPhotos();
            });

        $app->post(
            '/admin/tempsreponse_difficulte',
            function () use ($app, $adminapi) {
                $app->response->headers->set('Content-Type', 'application/json');
                $adminapi->difficulteTempsReponse();
            });
        $app->post(
            '/admin/set_tempsreponse_difficulte',
            function () use ($app, $adminapi) {
                $app->response->headers->set('Content-Type', 'application/json');
                $adminapi->setDifficulteTempsReponse();
            });

        $app->post(
            '/admin/zoom_difficulte',
            function () use ($app, $adminapi) {
                $app->response->headers->set('Content-Type', 'application/json');
                $adminapi->difficulteZoom();
            });
        $app->post(
            '/admin/set_zoom_difficulte',
            function () use ($app, $adminapi) {
                $app->response->headers->set('Content-Type', 'application/json');
                $adminapi->setDifficulteZoom();
            });

        $photoapi = new \Controller\PhotoController();
        $app->get(
            '/photo/:id',
            function ($id) use ($app, $photoapi){
                $app->response->headers->set('Content-Type', 'application/json');
                $photoapi->apiPhoto($id);
            })->name('detail_photo');

        $app->get(
            '/photos/:ville',
            function ($ville) use ($app, $photoapi) {
                $app->response->headers->set('Content-Type', 'application/json');
                $photoapi->photosVille($ville);
            });

        $difficulteapi = new \Controller\DifficulteController();
        $app->get(
            '/difficultes',
            function () use ($app, $difficulteapi){
                $app->response->headers->set('Content-Type', 'application/json');
                $difficulteapi->listDifApi();
            })->name('difficultes');

         $app->get(
            '/difficulte/:id',
            function ($id) use ($app, $difficulteapi){
                $app->response->headers->set('Content-Type', 'application/json');
                $difficulteapi->detailDifApi($id);
            })->name('difficulte');

        $villeapi = new \Controller\VilleController();
        $app->get(
            '/villes',
            function () use ($app, $villeapi){
                $app->response->headers->set('Content-Type', 'application/json');
                $villeapi->listVilleApi();
            })->name('villes');


        $app->get('/villes/:id', function($id) use ($app)
        {
            $app->response->headers->set('Content-Type', 'application/json');
            $c = new \Controller\VilleController();
            $c->detail($id);
        });
        $app->get('/villes/:nom', function($nom) use ($app)
        {
            $app->response->headers->set('Content-Type', 'application/json');
            $c = new \Controller\VilleController();
            $c->detail($nom);
        });



    });

$app->run();