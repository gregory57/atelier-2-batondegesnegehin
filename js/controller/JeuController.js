﻿var app = angular.module('app');

app.controller('JeuController', ['$scope', '$resource', '$routeParams', function($scope, $resource, $routeParams)
{
	$scope.charger_game = function(callback)
	{
		var token = localStorage.getItem('token') ;
		Game = $resource('play/games/' + $routeParams.id + "?token=" + token, null,
		{
			'query': {method:'GET', isArray:false} ,
			'update': {method:'PUT'	}
		});

		game = Game.query(function() 
		{

			var ville = game.ville;	
			Ville = $resource('api/villes/' + ville, null,
			{
				'query': {method:'GET', isArray:false}
			});

			var v = Ville.query(function()
			{
				$scope.nom_ville = v.nom;
				$scope.map = L.map('map', { zoomControl:false }).setView([v.latitude, v.longitude], game.zoom);
				L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
	    			attribution: '',
	    			minZoom: game.zoom,
	    			maxZoom: game.zoom,
    				
				}).addTo($scope.map);
				$("#map").height($(window).height());
				$scope.map.invalidateSize();		

				var lien = game.photos;
				
				Photo = $resource(lien, null,
				{
					'query': {method:'GET', isArray:false}
				});

				$scope.photos = Photo.query(function()
				{
			    	callback();
				});			
				
			});
		});
	};


	$scope.demarrer_partie = function()
	{
		$scope.score_total = 0;

		if(localStorage.getItem("etape") === null)
			$scope.etape(0);			
		else
		{
			$scope.score_total = parseInt(game.score);

			$scope.etape(parseInt(localStorage.getItem("etape")));			
		}

	};

	$scope.clic = function(e)
	{
		var latitude = e.latlng.lat;
		var longitude = e.latlng.lng;

		marker = L.marker([latitude, longitude]).addTo($scope.map);

		$scope.map.off('click');

		clearInterval(timer);

		$scope.calculer_score(latitude, longitude);
		
	}



	$scope.etape = function(id)
	{
		//Charger photo		


		

		$scope.map.on('click', $scope.clic);

		if($scope.photos[id] == undefined)
		{
			//Terminer partie

			$("#map").empty()
			localStorage.removeItem("etape");
			
			game.score = $scope.score_total;
			game.etat = 1;

			Game.update({id:game.id}, game);

			$("#resultat").append($("<h1 style='vertical-align:center'>").text("Terminé"));
			$("#resultat").append($("<h2>").text("Score final : "+$scope.score_total + " points"));
			$("#resultat").append($("<a href='scores'><button>").attr("class","btn btn-primary btn-lg").text("Meilleurs scores").on("click"));
			$("#resultat").append($("</a>"));
			$("#resultat").append($("<a href='#/creer_partie' style='margin-left:10px;'><button>").attr("class","btn btn-primary btn-lg").text("Recommencer").on("click"));
			$("#resultat").append($("</a>"));
		}
		else
		{
			$scope.photo = $scope.photos[id];
			$('#photo>img').css('display', '');
			
			localStorage.setItem("etape", id);
			$scope.current_etape = (parseInt(localStorage.getItem("etape")) + 1) + " / " + game.nb_photos;

			$scope.temps_reponse = game.temps_reponse;
			$scope.temps = 0.00;
			$scope.temps_percent = $scope.temps * 10;
			$scope.score_etape = 0;

			timer = setInterval(function()
				{
					$scope.temps +=0.1;
					$scope.$apply();
					$scope.temps_percent = ($scope.temps * 100)/$scope.temps_reponse;
					if ($scope.temps >= $scope.temps_reponse) {
						$scope.temps = $scope.temps_reponse;
						clearInterval(timer);
						$('#temps_progress').html('<div class="progress"><div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="'+$scope.temps.toFixed(2)+'" style="width: '+$scope.temps_percent.toFixed(2)+'%;max-width:100%">'+$scope.temps.toFixed(2)+' sec</div></div>');
						$('#temps_progress').append('<div class="alert alert-info"><strong>Temps écoulé !</strong><br />Vous avez dépassé le temps de réponse limite. Cette photo ne vous rapportera pas de points ...</div>');
					} else {
						$('#temps_progress').html('<div class="progress"><div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="'+$scope.temps.toFixed(2)+'" style="width: '+$scope.temps_percent.toFixed(2)+'%;max-width:100%">'+$scope.temps.toFixed(2)+' sec</div></div>');
					}
					
				}, 100);

		}


		
	};

	$scope.calculer_score = function(click_lat, click_lng)
	{			


		var real_lat = $scope.photo.latitude;
		var real_lng = $scope.photo.longitude;

		var distance = Math.round(L.latLng(real_lat, real_lng).distanceTo(L.latLng(click_lat, click_lng)));


		if(distance < game.distance)
			$scope.score_etape = 5;
		else if(distance < (game.distance * 2))
			$scope.score_etape = 3;
		else if(distance < (game.distance *3))
			$scope.score_etape = 1;
		else
			$scope.score_etape = 0;

		if($scope.temps <= ($scope.temps_reponse)/3)
			$scope.score_etape *= 3;
		else if($scope.temps <= ($scope.temps_reponse/2))
			$scope.score_etape *= 2;
		else if($scope.temps > $scope.temps_reponse)
			$scope.score_etape = 0;


		marker_reponse = L.marker([real_lat, real_lng]).addTo($scope.map);
		marker_reponse.bindPopup("<img class='miniature' src=\""+$scope.photos[localStorage.getItem("etape")].link + "\">"+"<h4>Distance : "+distance+" m</h4><h4>Temps : "+$scope.temps.toFixed(2)+" sec</h4><h4>Score : "+$scope.score_etape+" points</h4>").openPopup();

		var circle = L.circle([real_lat, real_lng], distance, {
	    	color: 'black',
	   	 	fillColor: 'lightgrey',
	    	fillOpacity: 0.3
		}).addTo($scope.map);


		$scope.score_total += $scope.score_etape;


		game.score = $scope.score_total;
		game.etat = 0; //toujours en cours

		Game.update({id:game.id}, game);

		
		localStorage.setItem("etape", parseInt(localStorage.getItem("etape")) + 1);

		$("#resultat").append($("<h2>").text("Distance : "+distance + " mètres"));
		$("#resultat").append($("<h2>").text("Score : "+$scope.score_etape + " points"));
		$("#resultat").append($("<h2>").text("Score total: "+$scope.score_total + " points"));
		$('#photo>img').css('display', 'none');
		$("#resultat").append($("<button>").attr("class","btn btn-primary btn-lg").text("Photo suivante").on("click", function()
			{
				$scope.map.removeLayer(marker);
				$scope.map.removeLayer(marker_reponse);
				$scope.map.removeLayer(circle);

				$("#resultat").empty();




				$scope.etape(parseInt(localStorage.getItem("etape")));
			}));






	};


	$scope.charger_game($scope.demarrer_partie);
	

}]);