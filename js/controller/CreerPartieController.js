var app = angular.module('app');


app.controller('CreerPartieController', ['$scope', '$resource', '$location', function($scope, $resource, $location)
{

	Ville = $resource("api/villes");

	$scope.villes = Ville.query();


	Difficulte = $resource("api/difficultes");

	$scope.difficultes = Difficulte.query();

	Partie = $resource("play/games/", null, 
	{
		'save':{method:'POST'}
	});

	if(localStorage.getItem("game") != null && localStorage.getItem("etape") != null)
	{
		$("body").append("<a id='reprendre' href='#/game/"+localStorage.getItem("game")+"'><button class='btn btn-danger' style='position:absolute;top:5px;right:5px'>Reprendre la partie</button></a>").on("click", function()
		{
			$("#reprendre").remove();
		})
	}


	$scope.creer = function()
	{	
		partie = 
		{
			'pseudo':$scope.pseudo,
			'ville':$scope.ville,
			'difficulte':$scope.difficulte
		}

		var res = Partie.save(partie, function()
			{
				localStorage.setItem('game', res.id);
				localStorage.setItem('token', res.token);	
				localStorage.removeItem("etape");			
				$location.url("/game/"+res.id);
			});
	}
}]);