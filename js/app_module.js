var app = angular.module('app', ['ngRoute', 'ngResource']);

app.config(['$routeProvider', function($routeProvider)
{
	$routeProvider
		.when("/creer_partie", 
		{
			templateUrl:'angular/creer_partie.html',
			controller:'CreerPartieController'

		})

		.when("/game/:id",
		{
			templateUrl:"angular/game.html",
			controller:'JeuController'

		})

		.otherwise(
		{
			redirectTo:"/creer_partie"
		})

}]);