<?php
namespace Controller;


class VilleController {
	public function listVilleApi(){
        $aff = array();
        $villes = new \Model\Ville();
        $vil = $villes->all();             	            
        foreach ($vil as $r) { 
        	list($latitude, $longitude) = explode(';',$r->geolocalisation);  
        	$aff[] = array( 'id' => $r->id, 'nom' => $r->nom, 'latitude' => $latitude, 'longitude' => $longitude);        	
        }
    echo json_encode($aff);
    }

    public function detail($id)
    {
        $out = array();
        $ville = \Model\Ville::find($id);
        try
        {
            if($ville == null)
                throw new \Exception("Ville inexistante");

            $out['id'] = $ville->id;
            $out['nom'] = $ville->nom;

            list($latitude, $longitude) = explode(';', $ville->geolocalisation);

            $out['latitude'] = $latitude;
            $out['longitude'] = $longitude;
        }
        catch (\Exception $e)
        {
            $out['statut'] = 'Error';
            $out['message'] = $e->getMessage();
        }
        echo json_encode($out);
    }
    public function detailNom($nom)
    {
        $out = array();
        $ville = \Model\Ville::where('nom','=',$nom)->get();
        try
        {
            if($ville == null)
                throw new \Exception("Ville inexistante");

            $out['id'] = $ville->id;
            $out['nom'] = $ville->nom;

            list($latitude, $longitude) = explode(';', $ville->geolocalisation);

            $out['latitude'] = $latitude;
            $out['longitude'] = $longitude;
        }
        catch (\Exception $e)
        {
            $out['statut'] = 'Error';
            $out['message'] = $e->getMessage();
        }
        echo json_encode($out);
    }
}