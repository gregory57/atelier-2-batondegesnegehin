<?php
namespace Controller;


class DifficulteController {
	public function listDifApi(){
        $aff = array();
        $difs = new \Model\Difficulte();
        $dif = $difs->all();                   	            
        foreach ($dif as $r) {  
        	$aff[] = array( 'id' => $r->id, 'difficulte' => $r->difficulte, 'distance' => $r->distance, 'nb_photos' => $r->nb_photos , 
                'temps reponse' => $r->temps_reponse,'zoom' => $r->zoom);        	
        }
    echo json_encode($aff);
    }

    public function detailDifApi($id){
        $aff = array();
        $difs = new \Model\Difficulte();
        $dif = $difs->find($id);                                
        $aff[] = array( 'id' => $dif->id, 'difficulte' => $dif->difficulte, 'distance' => $dif->distance, 'nb_photos' => $dif->nb_photos , 
            'temps reponse' => $dif->temps_reponse,'zoom' => $dif->zoom);           
    echo json_encode($aff);
    }
}