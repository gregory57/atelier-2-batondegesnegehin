<?php
namespace Controller;


class AdminController {
    public function login()
    {
        $loader = new \Twig_Loader_Filesystem('Template');
        $twig = new \Twig_Environment($loader, array('debug' => true));
        $tmpl = $twig->loadTemplate('admin/login_admin.html.twig') ;
        $tmpl->display(array());
    }

    public function logout()
    {
        if (isset($_SESSION['admin'])) {
            session_destroy();
            
        }
        $app = \Slim\Slim::getInstance() ;
        $app -> redirect("../admin") ;
    }

    public function verifLogin()
    {
        $login = $_POST['pseudo'];
        $mdp = md5($_POST['password']);
        $admin = new \Model\Admin();
        $result_admin = $admin->where('login', '=', $login)->get();
        if (isset($result_admin[0])) {
            if ($result_admin[0]->password == $mdp) {
                $_SESSION['admin'] = $result_admin[0]->login;
                $app = \Slim\Slim::getInstance() ;
                $app -> redirect("admin") ;
            }
        }
        $loader = new \Twig_Loader_Filesystem('Template');
        $twig = new \Twig_Environment($loader, array('debug' => true));
        $tmpl = $twig->loadTemplate('admin/login_admin.html.twig') ;
        $tmpl->display(array('error' => true));
    }

    public function register()
    {
        $loader = new \Twig_Loader_Filesystem('Template');
        $twig = new \Twig_Environment($loader, array('debug' => true));
        $tmpl = $twig->loadTemplate('admin/register_admin.html.twig') ;
        $tmpl->display(array());
    }

    public function verifRegister()
    {
        if (isset($_POST['pseudo'])) {
            $error = false;
            $message = "<ul>";
            $login = $_POST['pseudo'];
            $admin = new \Model\Admin();
            $result_admin = $admin->where('login', '=', $login)->get();
            if (isset($result_admin[0])) {
                $error = true;
                $message .= "<li>Ce pseudo est déjà utilisé.</li>";
            }
            if (strlen($_POST['password']) < 5) {
                $error = true;
                $message .= "<li>Le mot de passe doit être composé d'au moins 5 caractères.</li>";
            }
            if ($_POST['password'] != $_POST['passwordconfirm']) {
                $error = true;
                $message .= "<li>Les mots de passes ne sont pas identiques.</li>";
            }
            $message .= "</ul>";
            if ($error) {
                $loader = new \Twig_Loader_Filesystem('Template');
                $twig = new \Twig_Environment($loader, array('debug' => true));
                $tmpl = $twig->loadTemplate('admin/register_admin.html.twig') ;
                $tmpl->display(array("pseudo" => $login, "error" => $message));
            } else {
                $new_admin = new \Model\Admin();
                $new_admin->login = $login;
                $new_admin->password = md5($_POST['password']);
                $new_admin->save();
                $_SESSION['admin'] = $login;
                $app = \Slim\Slim::getInstance();
                $app -> redirect("../admin");
            }
            
        }
    }

    public function dashboard()
    {
        if (isset($_SESSION['admin'])) {
            $ville = new \Model\Ville();
            $liste_ville = $ville->all();
            $difficulte = new \Model\Difficulte();
            $liste_difficulte = $difficulte->all();
            $photos = array();
            $photo = new \Model\Photo();
            foreach ($liste_ville as $v) {
                $photos[$v->id] = $photo->where('id_ville', '=', $v->id)->count();
            }

            $loader = new \Twig_Loader_Filesystem('Template');
            $twig = new \Twig_Environment($loader, array('debug' => true));
            $tmpl = $twig->loadTemplate('admin/dashboard.html.twig') ;
            $tmpl->display(array('admin' => $_SESSION['admin'], 'villes' => $liste_ville, 'photos' => $photos, 'difficultes' => $liste_difficulte));
        }
    }
    public function actionDashboard()
    {
        $app = \Slim\Slim::getInstance();
        if (isset($_SESSION['admin'])) {
            if (isset($_POST['action'])) {
                switch ($_POST['action']) {
                    case 'photo':
                        $photo = new \Model\Photo();
                        $ville = new \Model\Ville();
                        $id = $photo->max('id') + 1;
                        $photo->id_ville = $_POST['ville'];
                        $photo->geolocalisation = $_POST['lat'].";".$_POST['lon'];
                        $path = $_FILES['photo_file']['name'];
                        $extension = pathinfo($path, PATHINFO_EXTENSION);
                        $villename = $ville->where('id', '=', $_POST['ville'])->get();
                        rename($_FILES['photo_file']['tmp_name'], "photo/".$villename[0]->nom."-".$id.".".$extension);
                        $photo->nom = $villename[0]->nom."-".$id.".".$extension;
                        $photo->save();
                        break;
                    
                    default:
                        # code...
                        break;
                }
            }
            
            $app->redirect("admin");
        }
    }

}