<?php
namespace Controller;

class GameController {
	public function index() 
	{
		$loader = new \Twig_Loader_Filesystem('Template');
        $twig = new \Twig_Environment($loader,
        array('debug' => true));
        $tmpl = $twig->loadTemplate('main.html.twig');
        $tmpl->display(array());
	}

	public function scores()
	{
		$partie = new \Model\Partie();
		$best_scores = $partie->orderBy('score', 'DESC')->take(10)->get();

		$ville = new \Model\Ville();
		$villes = $ville->all();
		$list_villes = array();
		foreach ($villes as $v) {
			$list_villes[$v->id] = $v->nom;
		}

		$loader = new \Twig_Loader_Filesystem('Template');
        $twig = new \Twig_Environment($loader,
        array('debug' => true));
        $tmpl = $twig->loadTemplate('scores.html.twig');
        $tmpl->display(array('scores' => $best_scores, 'ville' => $list_villes));
	}

	public function creerPartie()
	{
		$partie = new \Model\Partie();
		$out = array() ;		
		$list_photo = array() ;

		

		$json = json_decode(\Slim\Slim::getInstance()->request()->getBody());



		$partie->pseudo = $json->pseudo;
		$partie->id_ville = $json->ville;
		$partie->id_difficulte = $json->difficulte;

		$photo = new \Model\Photo();
		$photos = $photo -> where('id_ville', '=', $json->ville) 
						 -> orderByRaw("RAND() limit 10") 
						 -> get();
		foreach ($photos as $id_photo) {
			$list_photo[] = $id_photo->id;
		}
		$list_id_photos = implode(',', $list_photo);
		$partie->id_photos = $list_id_photos;

		$token = uniqid();
		$partie->token = $token;

		$partie->id_etat = 0;
		$partie->score = 0;
		$partie->save();

		$out['id'] = $partie->id;
		$out['token'] = $partie->token;
		echo json_encode($out);
	}

	public function description($id)
	{
	    $out = array();
		try
		{			
	        $p = \Model\Partie::with('ville', 'etat', 'difficulte')->get()->find($id);
	        $get = \Slim\Slim::getInstance()->request()->get();
	        if($p == null)
	        	throw new \Exception("Partie introuvable");

	        if(!isset($get['token']) or empty($get['token']) or ($get['token'] != $p->token))
	        	throw new \Exception("Token invalide");        	
	        
	        $out['id'] = $p->id;
	        $out['ville'] = $p->ville->id;
	        $out['etat'] = $p->etat->id;
	        $out['pseudo'] = $p->pseudo;
	        $out['score'] = $p->score;
	        $out['difficulte'] = $p->difficulte->difficulte;
	        $out['distance'] = $p->difficulte->distance;
	        $out['temps_reponse'] = $p->difficulte->temps_reponse;
	        $out['zoom'] = $p->difficulte->zoom;
	        $out['nb_photos'] = sizeof(explode(',', $p->id_photos));
	        $out['photos'] = \Slim\Slim::getInstance()->urlFor("photos", array("id"=>$id))."?token=".$p->token;
		}
		catch (\Exception $e)
		{
			$out['statut'] = "error";
			$out['message'] = $e->getMessage();
		}
	    
	    echo json_encode($out);   
	}

	public function update_partie($id)
	{

		$out = array();
		try 
		{			
			$p = \Model\Partie::find($id);
			$app = \Slim\Slim::getInstance();

			$json = json_decode(\Slim\Slim::getInstance()->request()->getBody());
			$get = \Slim\Slim::getInstance()->request()->get();




			if($p == null)
				throw new \Exception("Partie introuvable");
			
	        if(!isset($get['token']) or empty($get['token']) or ($get['token'] != $p->token))
	        	throw new \Exception("Token invalide");   	


			if(!isset($json->score) or !isset($json->etat) or !is_numeric($json->score) or !is_numeric($json->etat))
				throw new \Exception("Données invalides");

			$p->id_etat = $json->etat;
			$p->score = $json->score;

			

			$p->update();

			$out['statut'] = "success";
			$out['message'] = "Partie à jour";

		} 
		catch (\Exception $e) 
		{
			$out['statut'] = "error";
			$out['message'] = $e->getMessage();
		}

		echo json_encode($out);

	}

	public function bestScores()
	{
		$res = array();
		$partie = new \Model\Partie();
		$best_scores = $partie->orderBy('score', 'DESC')->take(10)->get();
		if ($best_scores) {
			$res['statut'] = 200;
			$res['scores'] = $best_scores;
		} else {
			$res['statut'] = 500;
		}
		echo json_encode($res);

	}

}