<?php
namespace Controller;


class PhotoController {
	public function apiListPhoto($id){
        $app = \Slim\Slim::getInstance();
        $correct_key = false;
        $ok_id = false;
        $id_pho = array();
        $aff = array();
        if (isset($_GET['token'])) {
        	$part = new \Model\Partie();
        	$p = $part->where('token','=',$_GET['token'])->get();
            if (!$p->isEmpty()) {
                $correct_key = true;
                $aff['status'] = 200;
                if($id == $p[0]->id){
                    $ok_id = true;           
                	$id_pho=explode(',',$p[0]->id_photos);           	            

            foreach ($id_pho as $r) {  
            	$pho = \Model\Photo::find($r);
            	list($latitude, $longitude) = explode(';',$pho->geolocalisation);
            	$aff[] = array( 'id' => $pho->id, 'nom' => $pho->nom, 'latitude' => $latitude, 'longitude' => $longitude , 'link' => "photo/".$pho->nom);        	
            }
        }
    }
    	}
        if(!$ok_id){
            $aff['status'] = 500;
            $aff['error'] = 'La partie n\'existe pas.';
        }
        if (!$correct_key) {
            $aff['status'] = 500;
            $aff['error'] = 'Token incorrect.';
        }
        echo json_encode($aff);
    }

    public function apiPhoto($id){
        $res = array();
        $correct_key = false;
        $ok_id = false;
        $photo = new \Model\Photo();
        $pho = $photo->where('id', '=', $id)->get();
            if (!$pho->isEmpty()) {
                $ok_id = true;
                $res['status'] = 200;
                $ville = new \Model\Ville();
                list($latitude, $longitude) = explode(';',$pho[0]->geolocalisation);
                $v = $ville->where('id','=',$pho[0]->id_ville)->get();
                $res[] = array( 'id' => $pho[0]->id, 'titre' => $pho[0]->nom, 'ville' => $v[0]->nom, 'latitude' => $latitude, 'longitude' => $longitude);
            }
        echo json_encode($res);
    }

    public function photosVille($ville)
    {
        $res = array();
        $photos = array();
        $photo = new \Model\Photo();
        $liste_photos = $photo->where('id_ville', '=', $ville)->get();
        if ($liste_photos) {
            $res['statut'] = 200;
        }
        foreach ($liste_photos as $pho) {
            list($latitude, $longitude) = explode(';',$pho->geolocalisation);
            $photos[] = array('id' => $pho->id, 'nom' => $pho->nom, 'latitude' => $latitude, 'longitude' => $longitude);
        }
        $res['photos'] = $photos;

        echo json_encode($res);
    }
}