<?php
namespace Controller;


class ApiAdminController {
    public function villeExist()
    {
        if (isset($_POST['ville'])) {
            $res = false;
            $villes = new \Model\Ville();
            $this_ville = $villes->where('nom', '=', $_POST['ville'])->get();
            if (isset($this_ville[0])) {
                $res = true;
            }
            echo json_encode(array('resultat' => $res));
        } else {
            echo json_encode(array('error' => 'Parametres manquants'));
        }
    }

    public function ajoutVille()
    {
        if (isset($_POST['ville']) && isset($_POST['lat']) && isset($_POST['lon'])) {
            $ville = $_POST['ville'];
            $new_ville = new \Model\Ville();
            $new_ville->nom = $ville;
            $new_ville->geolocalisation = $_POST['lat'].";".$_POST['lon'];
            $new_ville->save();
        }
    }

    public function difficulteDistance()
    {
        if (isset($_POST['difficulte'])) {
            $difficulte = new \Model\Difficulte();
            $this_difficulte = $difficulte->find($_POST['difficulte']);
            if ($this_difficulte) {
                echo json_encode(array('statut' => 200, 'resultat' => $this_difficulte->distance));
            } else {
                echo json_encode(array('statut' => 500, 'error' => 'Difficulte inconnue'));
            }
        } else {
            echo json_encode(array('statut' => 500, 'error' => 'Parametres manquants'));
        }
    }

    public function setDifficulteDistance()
    {
        if (isset($_SESSION['admin'])) {
            if (isset($_POST['difficulte']) && isset($_POST['distance'])) {
                $difficulte = new \Model\Difficulte();
                $this_difficulte = $difficulte->find($_POST['difficulte']);
                if ($this_difficulte) {
                    $this_difficulte->distance = $_POST['distance'];
                    $this_difficulte->save();
                    echo json_encode(array('statut' => 200));
                } else {
                    echo json_encode(array('statut' => 500, 'error' => 'Difficulte inconnue'));
                }
            } else {
                echo json_encode(array('statut' => 500, 'error' => 'Parametres manquants'));
            }
        } else {
            echo json_encode(array('statut' => 500, 'error' => 'Pas admin'));
        }
    }

    public function difficulteNbPhotos()
    {
        if (isset($_POST['difficulte'])) {
            $difficulte = new \Model\Difficulte();
            $this_difficulte = $difficulte->find($_POST['difficulte']);
            if ($this_difficulte) {
                echo json_encode(array('statut' => 200, 'resultat' => $this_difficulte->nb_photos));
            } else {
                echo json_encode(array('statut' => 500, 'error' => 'Difficulte inconnue'));
            }
        } else {
            echo json_encode(array('statut' => 500, 'error' => 'Parametres manquants'));
        }
    }

    public function setDifficulteNbPhotos()
    {
        if (isset($_SESSION['admin'])) {
            if (isset($_POST['difficulte']) && isset($_POST['nombre'])) {
                $difficulte = new \Model\Difficulte();
                $this_difficulte = $difficulte->find($_POST['difficulte']);
                if ($this_difficulte) {
                    $this_difficulte->nb_photos = $_POST['nombre'];
                    $this_difficulte->save();
                    echo json_encode(array('statut' => 200));
                } else {
                    echo json_encode(array('statut' => 500, 'error' => 'Difficulte inconnue'));
                }
            } else {
                echo json_encode(array('statut' => 500, 'error' => 'Parametres manquants'));
            }
        } else {
            echo json_encode(array('statut' => 500, 'error' => 'Pas admin'));
        }
    }

    public function difficulteTempsReponse()
    {
        if (isset($_POST['difficulte'])) {
            $difficulte = new \Model\Difficulte();
            $this_difficulte = $difficulte->find($_POST['difficulte']);
            if ($this_difficulte) {
                echo json_encode(array('statut' => 200, 'resultat' => $this_difficulte->temps_reponse));
            } else {
                echo json_encode(array('statut' => 500, 'error' => 'Difficulte inconnue'));
            }
        } else {
            echo json_encode(array('statut' => 500, 'error' => 'Parametres manquants'));
        }
    }

    public function setDifficulteTempsReponse()
    {
        if (isset($_SESSION['admin'])) {
            if (isset($_POST['difficulte']) && isset($_POST['temps'])) {
                $difficulte = new \Model\Difficulte();
                $this_difficulte = $difficulte->find($_POST['difficulte']);
                if ($this_difficulte) {
                    $this_difficulte->temps_reponse = $_POST['temps'];
                    $this_difficulte->save();
                    echo json_encode(array('statut' => 200));
                } else {
                    echo json_encode(array('statut' => 500, 'error' => 'Difficulte inconnue'));
                }
            } else {
                echo json_encode(array('statut' => 500, 'error' => 'Parametres manquants'));
            }
        } else {
            echo json_encode(array('statut' => 500, 'error' => 'Pas admin'));
        }
    }

    public function difficulteZoom()
    {
        if (isset($_POST['difficulte'])) {
            $difficulte = new \Model\Difficulte();
            $this_difficulte = $difficulte->find($_POST['difficulte']);
            if ($this_difficulte) {
                echo json_encode(array('statut' => 200, 'resultat' => $this_difficulte->zoom));
            } else {
                echo json_encode(array('statut' => 500, 'error' => 'Difficulte inconnue'));
            }
        } else {
            echo json_encode(array('statut' => 500, 'error' => 'Parametres manquants'));
        }
    }

    public function setDifficulteZoom()
    {
        if (isset($_SESSION['admin'])) {
            if (isset($_POST['difficulte']) && isset($_POST['zoom'])) {
                $difficulte = new \Model\Difficulte();
                $this_difficulte = $difficulte->find($_POST['difficulte']);
                if ($this_difficulte) {
                    $this_difficulte->zoom = $_POST['zoom'];
                    $this_difficulte->save();
                    echo json_encode(array('statut' => 200));
                } else {
                    echo json_encode(array('statut' => 500, 'error' => 'Difficulte inconnue'));
                }
            } else {
                echo json_encode(array('statut' => 500, 'error' => 'Parametres manquants'));
            }
        } else {
            echo json_encode(array('statut' => 500, 'error' => 'Pas admin'));
        }
    }
}