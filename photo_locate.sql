-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 12 Février 2015 à 16:26
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `photo_locate`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `admin`
--

INSERT INTO `admin` (`id`, `login`, `password`) VALUES
(1, 'admin', '2a1585a864d9e67627c6ae04c807a2c5');

-- --------------------------------------------------------

--
-- Structure de la table `difficulte`
--

CREATE TABLE IF NOT EXISTS `difficulte` (
  `id` int(1) NOT NULL,
  `difficulte` varchar(255) NOT NULL,
  `distance` float NOT NULL,
  `nb_photos` int(5) NOT NULL DEFAULT '10',
  `temps_reponse` int(5) NOT NULL DEFAULT '60',
  `zoom` float NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `difficulte`
--

INSERT INTO `difficulte` (`id`, `difficulte`, `distance`, `nb_photos`, `temps_reponse`, `zoom`) VALUES
(0, 'Facile', 300, 10, 60, 13),
(1, 'Normale', 200, 10, 30, 13),
(2, 'Difficile', 100, 10, 10, 13);

-- --------------------------------------------------------

--
-- Structure de la table `etat`
--

CREATE TABLE IF NOT EXISTS `etat` (
  `id` int(1) NOT NULL,
  `etat` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `etat`
--

INSERT INTO `etat` (`id`, `etat`) VALUES
(0, 'en cours'),
(1, 'terminer');

-- --------------------------------------------------------

--
-- Structure de la table `partie`
--

CREATE TABLE IF NOT EXISTS `partie` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `token` text NOT NULL,
  `id_ville` int(2) NOT NULL,
  `id_difficulte` int(1) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `id_etat` int(1) NOT NULL,
  `score` int(11) NOT NULL,
  `id_photos` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Contenu de la table `partie`
--



-- --------------------------------------------------------

--
-- Structure de la table `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ville` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `geolocalisation` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `photo`
--

INSERT INTO `photo` (`id`, `id_ville`, `nom`, `geolocalisation`) VALUES

(1, 0, 'Nancy-1.jpg', '48.695355;6.210537'),
(2, 0, 'Nancy-2.jpg', '48.693620;6.177769'),
(3, 0, 'Nancy-3.jpg', '48.654602;6.143489'),
(4, 0, 'Nancy-4.jpg', '48.689827;6.174351'),
(5, 0, 'Nancy-5.jpg', '48.682925;6.161080'),
(6, 0, 'Nancy-6.jpg', '48.646893;6.147845'),
(7, 0, 'Nancy-7.jpg', '48.680439;6.170593'),
(8, 0, 'Nancy-8.jpg', '48.663146;6.155283'),
(9, 0, 'Nancy-9.jpg', '48.698095;6.184528'),
(10, 0, 'Nancy-10.jpg', '48.693321;6.183515');

-- --------------------------------------------------------

--
-- Structure de la table `ville`
--

CREATE TABLE IF NOT EXISTS `ville` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `geolocalisation` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `ville`
--

INSERT INTO `ville` (`id`, `nom`, `geolocalisation`) VALUES
(0, 'Nancy', '48.688;6.1735'),
(1, 'Paris', '45.54871;2.25412');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
