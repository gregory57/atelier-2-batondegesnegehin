# Projet PhotoLocate

## Groupe

- Lucas GEHIN
- Alexis MAGRON
- Gregory BATON
- Guillaume DEGESNE

## Tableau de bord

Nous avons implémenté toutes les fonctionnalités demandées dans le cahier des charges. Un panneau d'administration permet d'ajouter des villes, des photos; de configurer (pour trois difficultés) , la distance D, le nombre de photos par partie, le temps de réponse limite et la valeur du zoom.

L'ordre de priorité et les assignations ont été regroupré sur un board Trello : ``https://trello.com/b/lJAjqFDr/atelier-2``

## Compte admin

L'inscription au panneau d'administration est libre mais il existe déjà un compte :

 - login : admin
 - password : final

## URL de test

``http://webetu.iutnc.univ-lorraine.fr/~magron5u/atelier``