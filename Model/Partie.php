<?php
namespace Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Partie extends Eloquent {
    protected $table = 'partie';
    protected $primaryKey = 'id';
    public $timestamps=false;

    public function ville()
    {
    	return $this->belongsTo('Model\Ville', 'id_ville');
    }

    public function etat()
    {
    	return $this->belongsTo('Model\Etat', 'id_etat');
    }

    public function difficulte()
    {
    	return $this->belongsTo('Model\Difficulte', 'id_difficulte');
    }

}