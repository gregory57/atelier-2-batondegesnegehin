<?php
namespace Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Admin extends Eloquent {
    protected $table = 'admin';
    protected $primaryKey = 'id';
    public $timestamps=false;
}