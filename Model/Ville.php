<?php
namespace Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Ville extends Eloquent {
    protected $table = 'ville';
    protected $primaryKey = 'id';
    public $timestamps=false;

    public function photos()
    {
    	return $this->hasMany('Model\Photo', 'id_ville');
    }
}