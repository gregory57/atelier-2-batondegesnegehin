<?php
namespace Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Difficulte extends Eloquent {
    protected $table = 'difficulte';
    protected $primaryKey = 'id';
    public $timestamps=false;
}