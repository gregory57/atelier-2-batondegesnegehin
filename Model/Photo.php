<?php
namespace Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Photo extends Eloquent {
    protected $table = 'photo';
    protected $primaryKey = 'id';
    public $timestamps=false;
}